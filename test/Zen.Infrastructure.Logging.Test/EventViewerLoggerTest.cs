﻿//using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Zen.Infrastructure.Logging.Interfaces;
//using Zen.Infrastructure.Logging.Loggers;

//namespace Zen.Infrastructure.Logging.Tests
//{
//    [TestClass]
//    public class EventViewerLoggerTest
//    {
//        private ILogger logger;

//        [TestInitialize]
//        public void Initialize()
//        {
//            logger = new EventViewerLogger("Application/ZNEOS", "8080","Accounts/EOS","zndev01");
//        }

//        [TestMethod]
//        public void DebugTest()
//        {
//            logger.Debug("This is a {logtype} spike test", "debug");
//        }

//        [TestMethod]
//        public void InformationTest()
//        {
//            logger.Information("This is a {logtype} spike test", "information");
//        }

//        [TestMethod]
//        public void WarningTest()
//        {
//            logger.Warning("This is a {logtype} spike test", "warning");
//        }

//        [TestMethod]
//        public void ErrorTest()
//        {
//            logger.Error("This is a {logtype} spike test", "error");
//        }

//        [TestMethod]
//        public void ExceptionTest()
//        {
//            logger.Error(new Exception("test exception"), "This is a {logtype} spike test", "exception");
//        }

//        [TestMethod]
//        public void NestLogsTest()
//        {
//            using (var inner = logger.Context("innerContext", "test value"))
//            {
//                inner.Information("Message with innerContext.");
//            }
//        }
//    }
//}
