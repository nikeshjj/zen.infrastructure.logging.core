﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zen.Infrastructure.Logging.Interfaces;


namespace Zen.Infrastructure.Logging.Tests
{
    [TestClass]
    public class GelfSpikeTests
    {
        private ILogger logger;

        [TestInitialize]
        public void Initialize()
        {
            //logger = LoggerFactory.GetLogger("gelf.tcp", "Collector.zsys.io", "13000", "UnitTest", "SpikeTest From TCP Unit Test");
            //logger = LoggerFactory.GetLogger("gelf.http", "http://Collector.zsys.io", "12202", "BuildMachine", "UnitTest");
            logger = LoggerFactory.GetLogger("gelf.udp", "5776a224-5e4e-464c-ae15-afbde3d6032d-ls.logit.io", "19737", "KPATIL", "ENV-KPATIL");
        }

        [TestMethod]
        public void DebugTest()
        {
            logger.Debug("This is a {errorloglevel} spike test", "debug");
        }

        [TestMethod]
        public void InformationTest()
        {
            logger.Information("This is a {0} spike test", "information");
        }

        [TestMethod]
        public void WarningTest()
        {
            logger.Warning("This is a {0} spike test", "warning");
        }

        [TestMethod]
        public void ErrorTest()
        {
            logger.Error("This is a {0} spike test", "error");
        }

        [TestMethod]
        public void ExceptionTest()
        {
            logger.Error(new Exception("test exception"), "This is a {0} spike test", "exception");
        }

        [TestMethod]
        public void NestLogsTest()
        {
            var exception = new Exception("test exception - main message",
                new Exception("test inner exception - 1",
                    new Exception("test inner exception - 2", new Exception("test inner exception - 3"))));

            logger.Error(exception, "This is a {0} spike test", "exception");
        }

        [TestMethod]
        public void NestLogsTestWithData()
        {
            var innerException3 = new Exception("test inner exception - 3");
            var innerException2 = new Exception("test inner exception - 2", innerException3);
            var innerException1 = new Exception("test inner exception - 1", innerException2);
            var exception = new Exception("test main exception", innerException1);
            exception.Data.Add("_userId", "-1");
         

            logger.Error(exception, "This is a {0} spike test", "exception");
        }

        [TestMethod]
        public void LoadTest()
        {
            for (var i = 1; i <= 1000; i++)
            {
                logger.Error("This is a load test. Count = {0}.", i);
            }
        }
    }
}
