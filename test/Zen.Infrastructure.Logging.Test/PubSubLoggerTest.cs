﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zen.Infrastructure.Logging.Interfaces;
using Zen.Infrastructure.Logging.Loggers;

namespace Zen.Infrastructure.Logging.Tests
{
    [TestClass]
    public class PubSubLoggerTest
    {
        private ILogger logger;

        [TestInitialize]
        public void Initialize()
        {
            logger = new PubSubLogger("Endpoint=sb://zqueued01.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=fMmkSaG2schc0z74oOzwsq3KZlMm051ZFEMdYp1Gk4o=", "Outbound.Logger", "Accounts","zndev01");
        }

        [TestMethod]
        public void DebugTest()
        {
            logger.Debug("This is a {logtype} spike1 test", "debug");
        }

        [TestMethod]
        public void InformationTest()
        {
            logger.Information("This is a {logtype} spike test", "information");
        }

        [TestMethod]
        public void WarningTest()
        {
            logger.Warning("This is a {logtype} spike test", "warning");
        }

        [TestMethod]
        public void ErrorTest()
        {
            logger.Error("This is a {logtype} spike test", "error");
        }

        [TestMethod]
        public void ExceptionTest()
        {
            logger.Error(new Exception("test exception"), "This is a {logtype} spike test", "exception");
        }

        [TestMethod]
        public void NestLogsTest()
        {
            using (var inner = logger.Context("innerContext", "test value"))
            {
                inner.Information("Message with innerContext.");
            }
        }
    }
}
