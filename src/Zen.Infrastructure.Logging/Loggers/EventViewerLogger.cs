﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Zen.Infrastructure.Logging.Interfaces;

namespace Zen.Infrastructure.Logging.Loggers
{
    public class EventViewerLogger :  ILogger
    {
        private readonly string _logSource;
        private readonly string _loggerRoot;
        private readonly string _port;
        private readonly string _logName;
        private readonly string _environment;

        public EventViewerLogger(string loggerRoot, string port, string logName, string environment)
        {
            _loggerRoot = loggerRoot;
            _port = port;
            _logName = logName;
            _environment = environment;
            _logSource = environment;
            if (!EventLog.SourceExists(_logSource))
            {
                EventLog.CreateEventSource(_logSource, logName);
            }
        }

        public void Dispose()
        {
        }

        public void Debug(string message)
        {
            ReplaceAndSend(message, EventLogEntryType.Information, null, null);
        }

        public void Information(string message)
        {
            ReplaceAndSend(message, EventLogEntryType.Information, null, null);
        }

        public void Warning(string message)
        {
            ReplaceAndSend(message, EventLogEntryType.Warning, null, null);
        }

        public void Error(string message)
        {
            ReplaceAndSend(message, EventLogEntryType.Error, null, null);
        }

        public void Fatal(string message)
        {
            ReplaceAndSend(message, EventLogEntryType.Error, null, null);
        }

        public void Debug(Exception exception, string message)
        {
            ReplaceAndSend(message, EventLogEntryType.Information, exception, null);
        }

        public void Information(Exception exception, string message)
        {
            ReplaceAndSend(message, EventLogEntryType.Information, exception, null);
        }

        public void Warning(Exception exception, string message)
        {
            ReplaceAndSend(message, EventLogEntryType.Warning, exception, null);
        }

        public void Error(Exception exception, string message)
        {
            ReplaceAndSend(message, EventLogEntryType.Error, exception, null);
        }

        public void Fatal(Exception exception, string message)
        {
            ReplaceAndSend(message, EventLogEntryType.Error, exception, null);
        }

        public void Debug(string format, params object[] args)
        {
            ReplaceAndSend(format, EventLogEntryType.Information, null, args);
        }

        public void Information(string format, params object[] args)
        {
            ReplaceAndSend(format, EventLogEntryType.Information, null, args);
        }

        public void Warning(string format, params object[] args)
        {
            ReplaceAndSend(format, EventLogEntryType.Warning, null, args);
        }

        public void Error(string format, params object[] args)
        {
            ReplaceAndSend(format, EventLogEntryType.Error, null, args);
        }

        public void Fatal(string format, params object[] args)
        {
            ReplaceAndSend(format, EventLogEntryType.Error, null, args);
        }

        public void Debug(Exception exception, string format, params object[] args)
        {
            ReplaceAndSend(format, EventLogEntryType.Information, exception, args);
        }

        public void Information(Exception exception, string format, params object[] args)
        {
            ReplaceAndSend(format, EventLogEntryType.Information, exception, args);
        }

        public void Warning(Exception exception, string format, params object[] args)
        {
            ReplaceAndSend(format, EventLogEntryType.Warning, exception, args);
        }

        public void Error(Exception exception, string format, params object[] args)
        {
            ReplaceAndSend(format, EventLogEntryType.Error, exception, args);
        }

        public void Fatal(Exception exception, string format, params object[] args)
        {
            ReplaceAndSend(format, EventLogEntryType.Error, exception, args);
        }

        public ILogger Context(string name, object value)
        {
            return new EventViewerLogger(_loggerRoot, _port, name + "." + _logName, _environment);
        }

        private void ReplaceAndSend(string template, EventLogEntryType level, Exception exception = null, params object[] args)
        {
            var tokens = new Dictionary<string, string> {{"level", ((int)level).ToString()}};
            string exceptionJson = null;
            var exceptionMessage =string.Empty;
            if (exception != null)
            {
                exceptionMessage = exception.Message;
                exceptionJson = JsonConvert.SerializeObject(exception);
            }

            if (string.IsNullOrWhiteSpace(template))
            {
                if (exception != null)
                {
                    EventLog.WriteEntry(_logSource, exceptionMessage, level, 0, 0, GetBytes(exceptionJson));
                }
                return;
            }
            if (args != null && args.Any())
            {
                try
                {
                    template = string.Format(template, args);
                }
                catch
                {
                    var regex = new Regex("{(.*?)}");
                    var splitStrings = regex.Split(template);
                    var i = 0;
                    foreach (var split in splitStrings)
                    {
                        if (Regex.IsMatch(template, "{" + split + "}", RegexOptions.IgnoreCase))
                        {
                            template = Regex.Replace(template, "{" + split + "}", args[i].ToString());
                            tokens.Add(split, args[i].ToString());
                            i++;
                        }
                    }
                }
            }
            if(null!= exceptionJson)
                EventLog.WriteEntry(_logSource, template, level, 0, 0, GetBytes(exceptionJson));
            else
            {
                EventLog.WriteEntry(_logSource, template, level, 0, 0);
            }
        }

        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

    }
}