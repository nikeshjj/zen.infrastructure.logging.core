﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Arebis.Logging.GrayLog;
using Newtonsoft.Json;
using Zen.Infrastructure.Logging.Common;
using Zen.Infrastructure.Logging.Interfaces;

namespace Zen.Infrastructure.Logging.Loggers
{
    public class GelfLogger : ILogger
    {
        /* Arebis.Logging.GrayLog
         * Ref : https://github.com/codetuner/Arebis.Logging.GrayLog
         */
        private readonly IGrayLogClient _logger;
        private readonly LogLevel _logLevel;
        public GelfLogger(IGrayLogClient logger, LogLevel logLevel)
        {
            _logger = logger;
            _logLevel = logLevel;
        }

        public void Dispose()
        {
            _logger.Dispose();
        }

        public void Debug(string message)
        {
            ReplaceAndSend(message, LogLevel.Debug, null, null);
        }

        public void Information(string message)
        {
            ReplaceAndSend(message, LogLevel.Informational, null, null);
        }

        public void Warning(string message)
        {
            ReplaceAndSend(message, LogLevel.Warning, null, null);
        }

        public void Error(string message)
        {
            ReplaceAndSend(message, LogLevel.Error, null, null);
        }

        public void Fatal(string message)
        {
            ReplaceAndSend(message, LogLevel.Emergency, null, null);
        }

        public void Debug(Exception exception, string message)
        {
            ReplaceAndSend(message, LogLevel.Debug, exception, null);
        }

        public void Information(Exception exception, string message)
        {
            ReplaceAndSend(message, LogLevel.Informational, exception, null);
        }

        public void Warning(Exception exception, string message)
        {
            ReplaceAndSend(message, LogLevel.Warning, exception, null);
        }

        public void Error(Exception exception, string message)
        {
            ReplaceAndSend(message, LogLevel.Error, exception, null);
        }

        public void Fatal(Exception exception, string message)
        {
            ReplaceAndSend(message, LogLevel.Emergency, exception, null);
        }

        public void Debug(string format, params object[] args)
        {
            ReplaceAndSend(format, LogLevel.Debug, null, args);
        }

        public void Information(string format, params object[] args)
        {
            ReplaceAndSend(format, LogLevel.Informational, null, args);
        }

        public void Warning(string format, params object[] args)
        {
            ReplaceAndSend(format, LogLevel.Warning, null, args);
        }

        public void Error(string format, params object[] args)
        {
            ReplaceAndSend(format, LogLevel.Error, null, args);
        }

        public void Fatal(string format, params object[] args)
        {
            ReplaceAndSend(format, LogLevel.Emergency, null, args);
        }

        public void Debug(Exception exception, string format, params object[] args)
        {
            ReplaceAndSend(format, LogLevel.Debug, exception, args);
        }

        public void Information(Exception exception, string format, params object[] args)
        {
            ReplaceAndSend(format, LogLevel.Informational, exception, args);
        }

        public void Warning(Exception exception, string format, params object[] args)
        {
            ReplaceAndSend(format, LogLevel.Warning, exception, args);
        }

        public void Error(Exception exception, string format, params object[] args)
        {
            ReplaceAndSend(format, LogLevel.Error, exception, args);
        }

        public void Fatal(Exception exception, string format, params object[] args)
        {
            ReplaceAndSend(format, LogLevel.Emergency, exception, args);
        }

        public ILogger Context(string name, object value)
        {
            return new GelfLogger(_logger, _logLevel);
        }

        private void ReplaceAndSend(string template, LogLevel level, Exception exception = null, params object[] args)
        {
            if (level <= _logLevel)
            {
                var tokens = new Dictionary<string, string> { { "level", ((int)level).ToString() } };
                string exceptionJson = null;
                var exceptionMessage = string.Empty;
                if (exception != null)
                {
                    exceptionMessage = exception.Message;
                    exceptionJson = JsonConvert.SerializeObject(exception);
                }

                if (string.IsNullOrWhiteSpace(template))
                {
                    if (exception != null)
                    {
                        _logger.Send(exceptionMessage, exceptionJson, tokens);
                    }
                    return;
                }
                if (args != null && args.Any())
                {
                    try
                    {
                        template = string.Format(template, args);
                    }
                    catch
                    {
                        var regex = new Regex("{(.*?)}");
                        var splitStrings = regex.Split(template);
                        var i = 0;
                        foreach (var split in splitStrings)
                        {
                            if (Regex.IsMatch(template, "{" + split + "}", RegexOptions.IgnoreCase))
                            {
                                template = Regex.Replace(template, "{" + split + "}", args[i].ToString());
                                tokens.Add(split, args[i].ToString());
                                i++;
                            }
                        }
                    }
                }
                _logger.Send(template, exceptionJson, tokens, exception);
            }
        }

    }
}
