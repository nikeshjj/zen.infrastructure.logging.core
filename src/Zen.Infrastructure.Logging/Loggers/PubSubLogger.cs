﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using Zen.Infrastructure.Logging.Common;
using Zen.Infrastructure.Logging.Interfaces;

namespace Zen.Infrastructure.Logging.Loggers
{
    public class PubSubLogger : ILogger
    {
        private static string _environment;
        private static string _host;
        private static string _logName;
        private static string _queuename;
        private readonly IDictionary<string, QueueClient> _pool = new Dictionary<string, QueueClient>();
        public readonly QueueClient Client;

        public PubSubLogger(string host, string queuename, string logName, string environment)
        {
            _host = host;
            _logName = logName;
            _environment = environment;
            _queuename = queuename;
            Client = CreateQueueClient();
        }
        
        public void Debug(string message)
        {
            ReplaceAndSend(message, LogLevel.Debug, null, null);
        }

        public void Information(string message)
        {
            ReplaceAndSend(message, LogLevel.Informational, null, null);
        }

        public void Warning(string message)
        {
            ReplaceAndSend(message, LogLevel.Warning, null, null);
        }

        public void Error(string message)
        {
            ReplaceAndSend(message, LogLevel.Error, null, null);
        }

        public void Fatal(string message)
        {
            ReplaceAndSend(message, LogLevel.Emergency, null, null);
        }

        public void Debug(Exception exception, string message)
        {
            ReplaceAndSend(message, LogLevel.Debug, exception, null);
        }

        public void Information(Exception exception, string message)
        {
            ReplaceAndSend(message, LogLevel.Informational, exception, null);
        }

        public void Warning(Exception exception, string message)
        {
            ReplaceAndSend(message, LogLevel.Warning, exception, null);
        }

        public void Error(Exception exception, string message)
        {
            ReplaceAndSend(message, LogLevel.Error, exception, null);
        }

        public void Fatal(Exception exception, string message)
        {
            ReplaceAndSend(message, LogLevel.Emergency, exception, null);
        }

        public void Debug(string format, params object[] args)
        {
            ReplaceAndSend(format, LogLevel.Debug, null, args);
        }

        public void Information(string format, params object[] args)
        {
            ReplaceAndSend(format, LogLevel.Informational, null, args);
        }

        public void Warning(string format, params object[] args)
        {
            ReplaceAndSend(format, LogLevel.Warning, null, args);
        }

        public void Error(string format, params object[] args)
        {
            ReplaceAndSend(format, LogLevel.Error, null, args);
        }

        public void Fatal(string format, params object[] args)
        {
            ReplaceAndSend(format, LogLevel.Emergency, null, args);
        }

        public void Debug(Exception exception, string format, params object[] args)
        {
            ReplaceAndSend(format, LogLevel.Debug, exception, args);
        }

        public void Information(Exception exception, string format, params object[] args)
        {
            ReplaceAndSend(format, LogLevel.Informational, exception, args);
        }

        public void Warning(Exception exception, string format, params object[] args)
        {
            ReplaceAndSend(format, LogLevel.Warning, exception, args);
        }

        public void Error(Exception exception, string format, params object[] args)
        {
            ReplaceAndSend(format, LogLevel.Error, exception, args);
        }

        public void Fatal(Exception exception, string format, params object[] args)
        {
            ReplaceAndSend(format, LogLevel.Emergency, exception, args);
        }

        public ILogger Context(string name, object value)
        {
            return new PubSubLogger(_host, _queuename, name, _environment);
        }

        public void Dispose()
        {
         
        }

        private void ReplaceAndSend(string message, LogLevel level, Exception exception = null, params object[] args)
        {
            var tokens = new Dictionary<string, string> { { "level", ((int)level).ToString() } };
            string exceptionJson = null;
            var exceptionMessage = string.Empty;
            if (exception != null)
            {
                exceptionMessage = exception.Message;
                exceptionJson = JsonConvert.SerializeObject(exception);
            }

            if (args != null && args.Any())
            {
                try
                {
                    message = string.Format(message, args);
                }
                catch
                {
                    var regex = new Regex("{(.*?)}");
                    var splitStrings = regex.Split(message);
                    var i = 0;
                    foreach (var split in splitStrings)
                    {
                        if (Regex.IsMatch(message, "{" + split + "}", RegexOptions.IgnoreCase))
                        {
                            message = Regex.Replace(message, "{" + split + "}", args[i].ToString());
                            tokens.Add(split, args[i].ToString());
                            i++;
                        }
                    }
                }
            }
            var properties = new Dictionary<string, object>
            {
                {"host",Environment.MachineName},
                {"level", level.ToString()},
                {"logName", _logName},
                {"environment", _environment},
                {"message", message},
                {"exceptionMessage", exceptionMessage},
                {"exceptionJson",exceptionJson }
            };
            Publish(properties, string.Empty);
        }

        #region Pub Sub
        
        private QueueClient CreateQueueClient()
        {
            lock (_pool)
            {
                QueueClient client;
                var token = $"{_queuename}::{10}";

                if (_pool.TryGetValue(token, out client) && client != null && !client.IsClosed)
                    return client;

                if (!Manager.QueueExists(_queuename))
                    Manager.CreateQueue(_queuename);

                client = QueueClient.CreateFromConnectionString(_host, _queuename);
                client.PrefetchCount = 10;
                _pool[token] = client;

                return client;
            }
        }

        private NamespaceManager _namespaceManager;

        private NamespaceManager Manager => _namespaceManager ??
                                            (_namespaceManager = NamespaceManager.CreateFromConnectionString(_host));

        private void Publish(IDictionary<string, object> properties, string payload)
        {
            var json = JsonConvert.SerializeObject(payload);
            var message = new BrokeredMessage(json);

            if (properties != null)
                foreach (var property in properties)
                {
                    message.Properties[property.Key] = property.Value;
                }
            Client.Send(message);
        }
        
        #endregion
    }
}