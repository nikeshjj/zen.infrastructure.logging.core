﻿using System.Net.Sockets;
using System.Text;

namespace Zen.Infrastructure.Logging.Loggers.GelfProviders
{
    public class GrayLogUdpClient : GrayLogBaseClient
    {
        public GrayLogUdpClient(string facility, string host, int port, string environment)
        {
            _port = port;
            _host = host;
            _facility = facility;
            _environment = environment;
        }

        public override void InternallySendMessage(string messageBody)
        {
            using (var client = new UdpClient())
            {
                client.Connect(_host, _port);
                var datagram = Encoding.ASCII.GetBytes(messageBody);
                client.Send(datagram, datagram.Length);
                client.Close();
            }
        }
    }
}
