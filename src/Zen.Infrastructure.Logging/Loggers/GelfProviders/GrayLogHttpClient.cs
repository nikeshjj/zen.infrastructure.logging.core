﻿using System.Net.Http;
using System.Text;

namespace Zen.Infrastructure.Logging.Loggers.GelfProviders
{
    public class GrayLogHttpClient : GrayLogBaseClient
    {
        public GrayLogHttpClient(string facility, string host, int port, string environment)
        {
            _port = port;
            _host = host;
            _facility = facility;
            _environment = environment;
            _url = @_host + ":" + _port + "/gelf";
        }


        public override void InternallySendMessage(string messageBody)
        {
            using (var client = new HttpClient())
            {
                //if (!string.IsNullOrEmpty(User) && !string.IsNullOrEmpty(Password))
                //{
                //    var byteArray = Encoding.ASCII.GetBytes(User + ":" + Password);
                //    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                //}
                var resourceAddress = _url;
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.ExpectContinue = false;
                var response = client.PostAsync(resourceAddress, new StringContent(messageBody, Encoding.UTF8, "application/json"));
                if (response.Result != null)
                {
                    var result = response.Result;
                }
            }
        }

    }
}
