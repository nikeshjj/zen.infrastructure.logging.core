﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Arebis.Logging.GrayLog;
using Newtonsoft.Json;

namespace Zen.Infrastructure.Logging.Loggers.GelfProviders
{
    public abstract class GrayLogBaseClient : IGrayLogClient
    {
        protected string _facility;
        protected string _host;
        protected int _port;
        protected string _url;
        protected string _environment;

        public void Dispose()
        {
            // Do Nothing
        }

        public void Send(Exception ex, SyslogLevel level = SyslogLevel.Error)
        {
            Send(ex);
        }

        public void Send(string shortMessage, string fullMessage = null, object data = null, Exception ex = null)
        {
            // Construct log record:
            var logRecord = new Dictionary<string, object>();
            logRecord["host"] = Environment.MachineName;
            logRecord["facility"] = _facility;
            logRecord["short_message"] = shortMessage;
            logRecord["_environment"] = _environment;
            if (!string.IsNullOrWhiteSpace(fullMessage))
                logRecord["full_message"] = fullMessage;

            if (data is string)
                logRecord["_data"] = data;
            else if (data is System.Collections.IDictionary)
                MergeDictionary(logRecord, (System.Collections.IDictionary)data, "_");
            else if (data is System.Collections.IEnumerable)
                logRecord["_values"] = data;
            else if (data != null)
                MergeObject(logRecord, data, "_");

            // Serialize object:
            var logRecordString = JsonConvert.SerializeObject(logRecord);
            // Dispatch message:
            InternallySendMessage(logRecordString);
        }

        public abstract void InternallySendMessage(string messageBody);

        public void Send(Exception ex)
        {
            // Collect exception data:
            var data = new System.Collections.Hashtable();
            for (var iex = ex; iex != null; iex = iex.InnerException)
            {
                foreach (var key in iex.Data.Keys)
                {
                    data.Add(key, iex.Data[key]);
                }
            }

            // Send exception:
            this.Send(ex.Message, ex.ToString(), data);
        }

        private void MergeDictionary(Dictionary<string, object> target, System.Collections.IDictionary source, string prefix)
        {
            foreach (var key in source.Keys)
            {
                target[prefix + key] = source[key];
            }
        }

        private static void MergeObject(IDictionary<string, object> target, dynamic source, string prefix = "")
        {
            foreach (PropertyInfo property in source.GetType().GetProperties())
            {
                target[prefix + property.Name] = property.GetValue(source);
            }
        }
    }
}
