﻿using System.Net.Sockets;
using System.Text;

namespace Zen.Infrastructure.Logging.Loggers.GelfProviders
{
    public class GrayLogTcpClient : GrayLogBaseClient
    {
        public GrayLogTcpClient(string facility, string host, int port, string environment)
        {
            _port = port;
            _host = host;
            _facility = facility;
            _environment = environment;
        }


        public override void InternallySendMessage(string messageBody)
        {
            using (var client = new TcpClient())
            {
                client.Connect(_host, _port);
                using (var stream = client.GetStream())
                {
                    var bytes = Encoding.ASCII.GetBytes(messageBody);
                    stream.Write(bytes, 0, bytes.Length);
                    var r = stream.DataAvailable;
                    stream.Close();
                }
                client.Close();
            }
        }
    }
}
