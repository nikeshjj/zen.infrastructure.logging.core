using System;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Elasticsearch;
using ILogger = Zen.Infrastructure.Logging.Interfaces.ILogger;

namespace Zen.Infrastructure.Logging.Loggers
{
    public class SeriLogLogger : ILogger
    {
        private readonly Serilog.ILogger _logger;

        public SeriLogLogger(string host, string logName)
        {
             _logger = new LoggerConfiguration()
                .Enrich.WithProperty("LogName", logName)
                .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(host))
                {
                    AutoRegisterTemplate = false, 
                })
                .MinimumLevel.Debug()
                .CreateLogger();
        }

        private SeriLogLogger(Serilog.ILogger logger)
        {
            this._logger = logger;
        }

        public SeriLogLogger(string host, string port, string logName, string environment)
        {
            var hostUrl = new Uri(host + ":" + port);
            if (environment == "local")
            {
                _logger = new LoggerConfiguration()
                    .Enrich.WithProperty("LogName", logName)
                    .Enrich.WithProperty("Environment", environment)
                    .WriteTo.LiterateConsole()
                    .MinimumLevel.Debug()
                    .CreateLogger();
            }
            else
            {
                _logger = new LoggerConfiguration()
                    .Enrich.WithProperty("LogName", logName)
                    .Enrich.WithProperty("Environment", environment)
                    .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(hostUrl)
                    {
                        AutoRegisterTemplate = false
                    })
                    .MinimumLevel.Debug()
                    .CreateLogger();
            }


        }

        public void Debug(string message)
        {
            if (_logger.IsEnabled(LogEventLevel.Debug))
            {
                _logger.Debug(message);
            }
        }
        public void Information(string message)
        {
            if (_logger.IsEnabled(LogEventLevel.Information))
            {
                _logger.Information(message);
            }
        }
        public void Warning(string message)
        {
            if (_logger.IsEnabled(LogEventLevel.Warning))
            {
                _logger.Warning(message);
            }
        }
        public void Error(string message)
        {
            if (_logger.IsEnabled(LogEventLevel.Error))
            {
                _logger.Error(message);
            }
        }
        public void Fatal(string message)
        {
            if (_logger.IsEnabled(LogEventLevel.Fatal))
            {
                _logger.Fatal(message);
            }
        }

        public void Debug(Exception exception, string message)
        {
            if (_logger.IsEnabled(LogEventLevel.Debug))
            {
                _logger.Debug(exception, message);
            }
        }
        public void Information(Exception exception, string message)
        {
            if (_logger.IsEnabled(LogEventLevel.Information))
            {
                _logger.Information(exception, message);
            }
        }
        public void Warning(Exception exception, string message)
        {
            if (_logger.IsEnabled(LogEventLevel.Warning))
            {
                _logger.Warning(exception, message);
            }
        }
        public void Error(Exception exception, string message)
        {
            if (_logger.IsEnabled(LogEventLevel.Error))
            {
                _logger.Error(exception, message);
            }
        }
        public void Fatal(Exception exception, string message)
        {
            if (_logger.IsEnabled(LogEventLevel.Fatal))
            {
                _logger.Fatal(exception, message);
            }
        }

        public void Debug(string format, params object[] args)
        {
            if (_logger.IsEnabled(LogEventLevel.Debug))
            {
                _logger.Debug(format, args);
            }
        }
        public void Information(string format, params object[] args)
        {
            if (_logger.IsEnabled(LogEventLevel.Information))
            {
                _logger.Information(format, args);
            }
        }
        public void Warning(string format, params object[] args)
        {
            if (_logger.IsEnabled(LogEventLevel.Warning))
            {
                _logger.Warning(format, args);
            }
        }
        public void Error(string format, params object[] args)
        {
            if (_logger.IsEnabled(LogEventLevel.Error))
            {
                _logger.Error(format, args);
            }
        }
        public void Fatal(string format, params object[] args)
        {
            if (_logger.IsEnabled(LogEventLevel.Fatal))
            {
                _logger.Fatal(format, args);
            }
        }

        public void Debug(Exception exception, string format, params object[] args)
        {
            if (_logger.IsEnabled(LogEventLevel.Debug))
            {
                _logger.Debug(exception, format, args);
            }
        }
        public void Information(Exception exception, string format, params object[] args)
        {
            if (_logger.IsEnabled(LogEventLevel.Information))
            {
                _logger.Information(exception, format, args);
            }
        }
        public void Warning(Exception exception, string format, params object[] args)
        {
            if (_logger.IsEnabled(LogEventLevel.Warning))
            {
                _logger.Warning(exception, format, args);
            }
        }
        public void Error(Exception exception, string format, params object[] args)
        {
            if (_logger.IsEnabled(LogEventLevel.Error))
            {
                _logger.Error(exception, format, args);
            }
        }
        public void Fatal(Exception exception, string format, params object[] args)
        {
            if (_logger.IsEnabled(LogEventLevel.Fatal))
            {
                _logger.Fatal(exception, format, args);
            }
        }

        public ILogger Context(string name, object value)
        {
            var innerLog = _logger.ForContext(name, value);
            return new SeriLogLogger(innerLog);
        }

        public void Dispose()
        {
            // do nothing
        }
    }
}