﻿using System;
using Zen.Infrastructure.Logging.Interfaces;

namespace Zen.Infrastructure.Logging
{
    public class Logger : ILogger
    {
        private readonly ILogger _provider;

        public Logger(string providerName, string host, string port, string logName, string environment)
        {
            _provider = LoggerFactory.GetLogger(providerName, host, port, logName, environment);
        }

        public void Dispose()
        {
            _provider.Dispose();
        }

        public void Debug(string message)
        {
            _provider.Debug(message);
        }

        public void Information(string message)
        {
            _provider.Information(message);
        }

        public void Warning(string message)
        {
            _provider.Warning(message);
        }

        public void Error(string message)
        {
            _provider.Error(message);
        }

        public void Fatal(string message)
        {
            _provider.Fatal(message);
        }

        public void Debug(Exception exception, string message)
        {
            _provider.Debug(exception, message);
        }

        public void Information(Exception exception, string message)
        {
            _provider.Information(exception, message);
        }

        public void Warning(Exception exception, string message)
        {
            _provider.Warning(exception, message);
        }

        public void Error(Exception exception, string message)
        {
            _provider.Error(exception, message);
        }

        public void Fatal(Exception exception, string message)
        {
            _provider.Fatal(exception, message);
        }

        public void Debug(string format, params object[] args)
        {
            _provider.Debug(format, args);
        }

        public void Information(string format, params object[] args)
        {
            _provider.Information(format, args);
        }

        public void Warning(string format, params object[] args)
        {
            _provider.Warning(format, args);
        }

        public void Error(string format, params object[] args)
        {
            _provider.Error(format, args);
        }

        public void Fatal(string format, params object[] args)
        {
            _provider.Fatal(format, args);
        }

        public void Debug(Exception exception, string format, params object[] args)
        {
            _provider.Debug(exception, format, args);
        }

        public void Information(Exception exception, string format, params object[] args)
        {
            _provider.Information(exception, format, args);
        }

        public void Warning(Exception exception, string format, params object[] args)
        {
            _provider.Warning(exception, format, args);
        }

        public void Error(Exception exception, string format, params object[] args)
        {
            _provider.Error(exception, format, args);
        }

        public void Fatal(Exception exception, string format, params object[] args)
        {
            _provider.Fatal(exception, format, args);
        }

        public ILogger Context(string name, object value)
        {
            return _provider.Context(name, value);
        }
    }
}
