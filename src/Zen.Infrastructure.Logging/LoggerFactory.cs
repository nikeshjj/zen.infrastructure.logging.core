﻿using System;
using Zen.Infrastructure.Logging.Common;
using Zen.Infrastructure.Logging.Interfaces;
using Zen.Infrastructure.Logging.Loggers;
using Zen.Infrastructure.Logging.Loggers.GelfProviders;

namespace Zen.Infrastructure.Logging
{
    public static class LoggerFactory
    {
        public static ILogger GetLogger(string providerName, string host, string port, string logName, string environment, LogLevel logLevel = LogLevel.Error)
        {
            switch (providerName.ToLower())
            {
                case "serilog":
                    return new SeriLogLogger(host, port, logName, environment);
                case "gelf.http":
                    return new GelfLogger(new GrayLogHttpClient(logName, host, Convert.ToInt32(port), environment), logLevel);
                case "gelf.tcp":
                    return new GelfLogger(new GrayLogTcpClient(logName, host, Convert.ToInt32(port), environment), logLevel);
                case "gelf.udp":
                    return new GelfLogger(new GrayLogUdpClient(logName, host, Convert.ToInt32(port), environment), logLevel);
                //case "eventlog":
                //    return new EventViewerLogger(host, port, logName, environment);
                case "pubsub":
                    return new PubSubLogger(host, port, logName, environment);
            }

            return null;
        }
    }
}
